class ChangeFieldType < ActiveRecord::Migration
  def up
    add_column :refinery_slider_images, :new_title, :text
    Refinery::SliderImages::SliderImage.find_each do |image|
      image.new_title = image.title
      image.save
    end
    remove_column :refinery_slider_images, :title
    rename_column :refinery_slider_images, :new_title, :title
  end

  def down

  end
end
