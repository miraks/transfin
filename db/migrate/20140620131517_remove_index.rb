class RemoveIndex < ActiveRecord::Migration
  def up
    remove_index :refinery_user_plugins, column: [:user_id, :name]
  end

  def down
    add_index :refinery_user_plugins, [:user_id, :name]
  end
end
