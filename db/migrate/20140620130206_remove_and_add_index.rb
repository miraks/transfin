class RemoveAndAddIndex < ActiveRecord::Migration
  def up
    remove_index :refinery_user_plugins, column: [:user_id, :name]
    add_index :refinery_user_plugins, [:user_id, :name]
  end

  def down
    remove_index :refinery_user_plugins, column: [:user_id, :name]
    add_index :refinery_user_plugins, [:user_id, :name]
  end
end
