$ ->
  $slider = $('#homeslider')
  return if $slider.length == 0
  $slider = $slider.royalSlider
    addActiveClass: true
    arrowsNav: true
    arrowsNavAutoHide: false
    # autoScaleSlider: true
    autoScaleSliderHeight: 1000
    imageScalePadding: 0
    loop: true
    keyboardNavEnabled: true
    slidesSpacing: 0
    visibleNearby:
      enabled: true
      centerArea: 0.6
      # center: true
      # breakpoint: 650
      # breakpointCenterArea: 0.64
      # navigateByCenterClick: true
