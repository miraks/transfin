Refinery::Pages::MenuPresenter.class_eval do
  config_accessor :wrapper_tag, :wrapper_class

  self.dom_id = nil
  self.css = 'main-nav'
  self.menu_tag = :nav
  self.wrapper_tag = :div
  self.wrapper_class = :wrapper
  self.list_tag = :ul
  self.list_item_tag = :li
  self.selected_css = :active
  self.first_css = nil
  self.last_css = nil

  def render_menu(items)
    content_tag(menu_tag, :id => dom_id, :class => css) do
      content_tag(wrapper_tag, class: wrapper_class) do
        render_menu_items(items)
      end
    end
  end

  def render_menu_item(menu_item, index)
    content_tag(list_item_tag, :class => menu_item_css(menu_item, index)) do
      buffer = ActiveSupport::SafeBuffer.new
      buffer << link_to(menu_item.title, context.refinery.url_for(menu_item.url), class: (selected_css if selected_item_or_descendant_item_selected?(menu_item)))
      buffer << render_menu_items(menu_item_children(menu_item))
      buffer
    end
  end
end
