Refinery::PagesController.class_eval do
  before_filter :find_last_events, only: :home
  before_filter :find_slides, only: :home

  private

  def find_last_events
    @events = Refinery::Events::Event.last(2).reverse
  end

  def find_slides
    @slides = Refinery::SliderImages::SliderImage.scoped
  end
end
