module Refinery
  module Events
    class Event < Refinery::Core::BaseModel
      self.table_name = 'refinery_events'

      attr_accessible :title, :date, :text, :position

      validates :title, :presence => true, :uniqueness => true

      default_scope -> { order 'position desc' }
    end
  end
end
