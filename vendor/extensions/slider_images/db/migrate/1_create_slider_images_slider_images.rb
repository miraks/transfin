class CreateSliderImagesSliderImages < ActiveRecord::Migration

  def up
    create_table :refinery_slider_images do |t|
      t.string :title
      t.integer :image_id
      t.integer :position

      t.timestamps
    end

  end

  def down
    if defined?(::Refinery::UserPlugin)
      ::Refinery::UserPlugin.destroy_all({:name => "refinerycms-slider_images"})
    end

    if defined?(::Refinery::Page)
      ::Refinery::Page.delete_all({:link_url => "/slider_images/slider_images"})
    end

    drop_table :refinery_slider_images

  end

end
