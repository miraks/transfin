# encoding: utf-8
require "spec_helper"

describe Refinery do
  describe "SliderImages" do
    describe "Admin" do
      describe "slider_images" do
        refinery_login_with :refinery_user

        describe "slider_images list" do
          before do
            FactoryGirl.create(:slider_image, :title => "UniqueTitleOne")
            FactoryGirl.create(:slider_image, :title => "UniqueTitleTwo")
          end

          it "shows two items" do
            visit refinery.slider_images_admin_slider_images_path
            page.should have_content("UniqueTitleOne")
            page.should have_content("UniqueTitleTwo")
          end
        end

        describe "create" do
          before do
            visit refinery.slider_images_admin_slider_images_path

            click_link "Add New Slider Image"
          end

          context "valid data" do
            it "should succeed" do
              fill_in "Title", :with => "This is a test of the first string field"
              click_button "Save"

              page.should have_content("'This is a test of the first string field' was successfully added.")
              Refinery::SliderImages::SliderImage.count.should == 1
            end
          end

          context "invalid data" do
            it "should fail" do
              click_button "Save"

              page.should have_content("Title can't be blank")
              Refinery::SliderImages::SliderImage.count.should == 0
            end
          end

          context "duplicate" do
            before { FactoryGirl.create(:slider_image, :title => "UniqueTitle") }

            it "should fail" do
              visit refinery.slider_images_admin_slider_images_path

              click_link "Add New Slider Image"

              fill_in "Title", :with => "UniqueTitle"
              click_button "Save"

              page.should have_content("There were problems")
              Refinery::SliderImages::SliderImage.count.should == 1
            end
          end

        end

        describe "edit" do
          before { FactoryGirl.create(:slider_image, :title => "A title") }

          it "should succeed" do
            visit refinery.slider_images_admin_slider_images_path

            within ".actions" do
              click_link "Edit this slider image"
            end

            fill_in "Title", :with => "A different title"
            click_button "Save"

            page.should have_content("'A different title' was successfully updated.")
            page.should have_no_content("A title")
          end
        end

        describe "destroy" do
          before { FactoryGirl.create(:slider_image, :title => "UniqueTitleOne") }

          it "should succeed" do
            visit refinery.slider_images_admin_slider_images_path

            click_link "Remove this slider image forever"

            page.should have_content("'UniqueTitleOne' was successfully removed.")
            Refinery::SliderImages::SliderImage.count.should == 0
          end
        end

      end
    end
  end
end
