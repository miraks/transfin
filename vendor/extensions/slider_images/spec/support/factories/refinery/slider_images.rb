
FactoryGirl.define do
  factory :slider_image, :class => Refinery::SliderImages::SliderImage do
    sequence(:title) { |n| "refinery#{n}" }
  end
end

