module Refinery
  module SliderImages
    class Engine < Rails::Engine
      extend Refinery::Engine
      isolate_namespace Refinery::SliderImages

      engine_name :refinery_slider_images

      before_inclusion do
        Refinery::Plugin.register do |plugin|
          plugin.name = "slider_images"
          plugin.url = proc { Refinery::Core::Engine.routes.url_helpers.slider_images_admin_slider_images_path }
          plugin.pathname = root
          plugin.activity = {
            :class_name => :'refinery/slider_images/slider_image'
          }
          
        end
      end

      config.after_initialize do
        Refinery.register_extension(Refinery::SliderImages)
      end
    end
  end
end
