module Refinery
  module SliderImages
    class SliderImage < Refinery::Core::BaseModel
      self.table_name = 'refinery_slider_images'

      attr_accessible :title, :image_id, :position

      belongs_to :image

      validates :title, :presence => true, :uniqueness => true

      default_scope -> { order 'position desc' }
    end
  end
end
