module Refinery
  module SliderImages
    class SliderImagesController < ::ApplicationController

      before_filter :find_all_slider_images
      before_filter :find_page

      def index
        # you can use meta fields from your model instead (e.g. browser_title)
        # by swapping @page for @slider_image in the line below:
        present(@page)
      end

      def show
        @slider_image = SliderImage.find(params[:id])

        # you can use meta fields from your model instead (e.g. browser_title)
        # by swapping @page for @slider_image in the line below:
        present(@page)
      end

    protected

      def find_all_slider_images
        @slider_images = SliderImage.order('position ASC')
      end

      def find_page
        @page = ::Refinery::Page.where(:link_url => "/slider_images").first
      end

    end
  end
end
