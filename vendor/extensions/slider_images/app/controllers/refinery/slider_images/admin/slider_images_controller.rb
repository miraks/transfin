module Refinery
  module SliderImages
    module Admin
      class SliderImagesController < ::Refinery::AdminController

        crudify :'refinery/slider_images/slider_image',
                :xhr_paging => true

      end
    end
  end
end
