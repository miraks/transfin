# Slider Images extension for Refinery CMS.

## How to build this extension as a gem

    cd vendor/extensions/slider_images
    gem build refinerycms-slider_images.gemspec
    gem install refinerycms-slider_images.gem

    # Sign up for a http://rubygems.org/ account and publish the gem
    gem push refinerycms-slider_images.gem