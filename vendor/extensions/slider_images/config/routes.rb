Refinery::Core::Engine.routes.draw do

  # Frontend routes
  namespace :slider_images do
    resources :slider_images, :path => '', :only => [:index, :show]
  end

  # Admin routes
  namespace :slider_images, :path => '' do
    namespace :admin, :path => Refinery::Core.backend_route do
      resources :slider_images, :except => :show do
        collection do
          post :update_positions
        end
      end
    end
  end

end
