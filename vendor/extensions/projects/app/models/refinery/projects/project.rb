module Refinery
  module Projects
    class Project < Refinery::Core::BaseModel
      self.table_name = 'refinery_projects'

      attr_accessible :title, :text, :image_id, :position
      belongs_to :image

      validates :title, :presence => true, :uniqueness => true

      default_scope -> { order 'position desc' }
    end
  end
end
