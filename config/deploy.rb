set :application, 'transfin'
set :repo_url, 'git@bitbucket.org:miraks/transfin.git'

set :branch, :master

set :deploy_to, '/home/hosting_sactiv/projects/transfin'
set :scm, :git

set :format, :pretty
set :log_level, :info

set :linked_files, %w{config/database.yml}
set :linked_dirs, %w{log tmp public/system}

set :keep_releases, 10

set :bundle_bins, fetch(:bundle_bins) + %w{ruby}

set :rvm_ruby_version, '2.2.0'

set :puma_bind, "unix:///var/sockets/hosting_sactiv/transfin.sactiv.sock"
set :puma_threads, [4, 16]
set :puma_init_active_record, true
