set :stage, :production

set :rails_env, 'production'

role :app, %w{hosting_sactiv@sulfur.locum.ru}
role :web, %w{hosting_sactiv@sulfur.locum.ru}
role :db,  %w{hosting_sactiv@sulfur.locum.ru}

server 'sulfur.locum.ru', user: 'hosting_sactiv', roles: %w{web app db}
